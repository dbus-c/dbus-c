#! /bin/sh

failures=0
successes=0
fail () {
	printf "Failed %s\n" "$@" 1>&2
	failures=$((failures + 1))
	continue
}

printf '' > all.pretty.expected
for inputfile in *.xml; do
	dbus-binding-tool \
		--mode=pretty "$inputfile" \
		--output="$inputfile.dummy" \
		> "$inputfile.pretty.expected" \
		|| fail "GLib pretty printing for $inputfile"
	rm -f "$inputfile.dummy"

	../dbus-c-binding-tool/dbus-c-binding-tool \
		--mode=pretty \
		"$inputfile" \
		> "$inputfile.pretty.real" \
		|| fail "dbus-c-binding-tool $inputfile"
	diff -u "$inputfile.pretty.expected" "$inputfile.pretty.real" \
		|| fail "pretty of $inputfile: not equal to GLib"
	successes=$((successes + 1))

	../dbus-c-binding-tool/dbus-c-binding-tool \
		--mode=pretty \
		"$inputfile" \
		--output "$inputfile.pretty.real" \
		|| fail "dbus-c-binding-tool $inputfile with --output"
	diff -u "$inputfile.pretty.expected" "$inputfile.pretty.real" \
		|| fail "pretty of $inputfile: not equal to GLib"
	successes=$((successes + 1))

	cat $inputfile.pretty.real >> all.pretty.expected
done

for i in once; do
	../dbus-c-binding-tool/dbus-c-binding-tool \
		--mode=pretty \
		*.xml \
		--output "all.pretty.real" \
		|| fail "dbus-c-binding-tool all with --output"
	diff -u "all.pretty.expected" "all.pretty.real" \
		|| fail "pretty of all: not equal to individuals"
	successes=$((successes + 1))
done

printf "Failed %d; Success %d\n" $failures $successes
