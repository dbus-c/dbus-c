/* dbus-c.h  Convenience header including all other headers
 *
 * Copyright (C) 2014 The dbus-c authors
 *
 * Licensed under your choice of the MIT license, the Academic Free License
 * version 2.1, or the GNU General Public License version 2 (or, at your
 * option any later version).
 *
 * ---
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ---
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifndef DBUS_C_H
#define DBUS_C_H

/**
 * @defgroup dbus-c D-Bus high-level C API
 * @brief A high-level C API for D-Bus
 *
 * dbus-c is a C binding for D-Bus. It is a very thin layer on top the of the
 * D-Bus low-level API. Unlike the low-level API, dbus-c uses C99 types in its
 * API. It also relies on pthreads.
 *
 * dbus-c is mainly meant for smaller applications that don't use a big library
 * like GLib or Qt. It is therefore meant to be simple rather than complete or
 * fully flexible.
 * 
 * @{
 */

/** @} */

/**
 * @mainpage
 *
 */

#endif /* DBUS_C_H */
